# glabit-docs

[[_TOC_]]

## Overview

The Provisionr documentation provides the engineering and technical documentation for the [provisionr-app](https://gitlab.com/provisionr/provisionr-app).

We use GitLab Pages to publish this to [https://provisionr.app](https://provisionr.app) where users can read the documentation with the rendered HTML.

See the [provisionr-app](https://gitlab.com/provisionr/provisionr-app/-/issues) project issue tracker for development, feature requests, and bug reports. Any documentation-specific issues can be created in the [provisionr-docs](https://gitlab.com/provisionr/provisionr-docs/-/issues) project. We encourage you to contribute with a merge request and assign it to a maintainer to review.

## How It Works

The Provisionr documentation site uses the [Docusaurus](https://docusaurus.io/) framework as our static site generator that is hosted using [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).

### Repository Location

The `provisionr-docs` repository is at [gitlab.com/provisionr/provisionr-docs](https://gitlab.com/provisionr/provisionr-docs).

### Framework Decision

There are many static site generators available (see the [blog](https://about.gitlab.com/blog/2022/04/18/comparing-static-site-generators/) or list of [GitLab Pages examples](https://gitlab.com/pages)).

After testing Hugo, Gatsby, Gitbook, Eleventy, Jigsaw, and Nuxt, we found that the Docusaurus framework fit our needs best for integration with GitLab CI pipelines for compiling and testing, integration with [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), Markdown support and plugins, [library of inspiration](https://docusaurus.io/showcase), turnkey CSS styling with support for the top-level, left sidebar with nesting links, and right sidebar table of contents.

### Code Structure

The GitLab repository that you're viewing has a top-level `website` directory.

#### Configuration

- `docusaurus.config.js` - This has the core configuration of the website and behaves similar to an environment variables file.
- `sidebars.js` - This has the configuration for the links in the top and left sidebars. Child pages are automatically rendered in the sidebar.

#### Markdown

You'll find all of the Markdown files in the `website/docs` directory that you can contribute to using GitLab MRs (just like any other project).

You can read the Docusaurus documentation or use common development practices to copy what the existing pages in the folder look like.

As part of our values we believe in clean up over sign off so feel free to contribute since some documentation is better than no documentation. We'll iterate it as we go. Feel free to clean up any of the formatting, grammar problems, or typos that you see.

Don't worry about making mistakes! Any Docusaurus-isms that you miss will be caught by the CI pipeline test job or the maintainers in the MR review.

See [CONTRIBUTING.md](CONTRIBUTING.md) for more details.

#### Other

- `src` and `static` - These are a few custom styles and HTML assets that override the components that are automagically compiled by Node.js with the packages in `package.json`. You don't need to worry about this.
- `src/css/custom.css` for Tailwind CSS modifications. You don't need to make changes for 99% of contributions.

See the [Docusaurus documentation](https://docusaurus.io/docs/category/guides) to learn more.

### CI Pipelines and Hosting

#### Published Changes

Every change in a branch or MR has a testing job that performs linting to check for broken links, unrenderable pages, etc. The GitLab Pages site is not updated until the change is merged onto the `main` branch.

You can see the [CONTRIBUTING.md](CONTRIBUTING.md) guide for using Docusaurus locally to see your changes in real time.

After the MR has been merged to `main` branch, the updates are automatically published to GitLab pages and will be visible on the website a few seconds after the pipeline finishes.

> If your browser has cached the content, you might need to refresh or open the page in an incognito window to see changes.

#### Domain Names

The GitLab Pages platform hosts all website on `gitlab.io` by default. Your top-level group in GitLab is referred to as your namespace and is a sub-domain for GitLab Pages. All child groups and repository names become URL paths.

In other words, the schema look like `{namespace}.gitlab.io/{child_group}/{grandchild_group}/{repo_name}`. We have configured a custom domain to avoid these paths and can see the docs at [https://provisionr.app](https://provisionr.app).

#### Domains for Other Projects

Does this documentation look good and you want to use it for other GitLab projects with Markdown content?

If we were using GitLab Pages from a deeply nested project (ex. `https://gitlab.com/{namespace}/{child_group}/{grandchild_group}/{repo_name}`), the URL will be `https://{namespace}.gitlab.io/{child_group}/{grandchild_group}/{repo_name}`. This is hard to remember and share.

It also gets more difficult with the paths to pages in your documentation site (ex. `https://{namespace}.gitlab.io/{child_group}/{grandchild_group}/{repo_name}/architecture/overview`).

It is recommended to use a top-level group to make the subdomain have the branding of your department.

### Maintainers

| Name | GitLab Handle | Email |
|------|---------------|-------|
| Jeff Martin | [@jeffersonmartin](https://gitlab.com/jeffersonmartin) | `jmartin@provisionr.ai` |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) to learn more about how to contribute.
