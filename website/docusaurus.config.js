// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require('prism-react-renderer/themes/github');
const darkCodeTheme = require('prism-react-renderer/themes/dracula');

/** @type {import('@docusaurus/types').Config} */
const config = {
    title: 'Provisionr Docs',
    tagline: 'Open source self-service automation platform for dev/test, sandbox, and training cloud infrastructure provisioning with streamlined role based access control (RBAC).',
    favicon: 'img/favicon.ico',
    url: 'https://provisionr.app/',
    baseUrl: '/',
    i18n: {
        defaultLocale: 'en',
        locales: ['en'],
    },
    onBrokenLinks: 'throw',
    onBrokenMarkdownLinks: 'warn',
    markdown: {
        mermaid: true,
    },
    presets: [
        [
            'classic',
            /** @type {import('@docusaurus/preset-classic').Options} */
            ({
                docs: {
                    routeBasePath: '/',
                    sidebarPath: require.resolve('./sidebars.js'),
                    editUrl: 'https://gitlab.com/provisionr/provisionr-docs/-/blob/main/website/docs/',
                },
                blog: false,
                theme: {
                    customCss: require.resolve('./src/css/custom.css'),
                },
            }),
        ],
    ],
    themeConfig:
        /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
        ({
            // Replace with your project's social card
            image: 'img/provisionr-docs-social-card.png',
            prism: {
                theme: require('prism-react-renderer/themes/github'),
                darkTheme: require('prism-react-renderer/themes/palenight'),
                additionalLanguages: ['php'],
                defaultLanguage: 'php'
            },
            navbar: {
                hideOnScroll: false,
                title: 'Provisionr Docs',
                logo: {
                    alt: 'Provisionr Logo',
                    src: 'img/logo.svg'
                },
                items: [
                    { type: 'doc', position: 'left', docId: 'aws/overview', label: 'AWS' },
                    { type: 'doc', position: 'left', docId: 'directory/overview', label: 'Directory' },
                    { type: 'doc', position: 'left', docId: 'gcp/overview', label: 'GCP' },
                    { type: 'doc', position: 'left', docId: 'gitlab/overview', label: 'GitLab' },
                    { type: 'doc', position: 'left', docId: 'training/overview', label: 'Training Labs' },
                    { type: 'doc', position: 'right', docId: 'api/overview', label: 'API' },
                    { type: 'doc', position: 'right', docId: 'architecture/overview', label: 'Architecture' },
                    { position: 'right', label: 'Changelog', href: 'https://gitlab.com/provisionr/provisionr-app/-/tree/main/changelog' },
                    { type: 'doc', position: 'right', docId: 'contributor/overview', label: 'Contributor Guide' },
                    { position: 'right', label: 'GitLab', href: 'https://gitlab.com/provisionr/provisionr-docs' }
                ],
            },
            footer: {
                style: 'dark',
                links: [
                    {
                        title: 'User Documentation',
                        items: [
                            {label: 'AWS', to: '/aws/overview'},
                            {label: 'Directory', to: '/directory/overview'},
                            {label: 'GCP', to: '/gcp/overview'},
                            {label: 'GitLab', to: '/gitlab/overview'},
                            {label: 'Training Labs', to: '/training/overview'},
                        ],
                    },
                    {
                        title: 'Admin Documentation',
                        items: [
                            {label: 'API Reference', to: '/api/overview'},
                            {label: 'Architecture', to: '/architecture/overview'},
                            {label: 'Contributor Guide', to: '/contributor/overview'}
                        ],
                    },
                    {
                        title: 'Repositories',
                        items: [
                            {label: 'provisionr-app', href: 'https://gitlab.com/provisionr/provisionr-app'},
                            {label: 'provisionr-docs', href: 'https://gitlab.com/provisionr/provisionr-docs'},
                        ],
                    },
                    {
                        title: 'Support',
                        items: [
                            {label: 'Releases', href: 'https://gitlab.com/provisionr/provisionr-app/-/tags'},
                            {label: 'Roadmap', href: 'https://gitlab.com/provisionr/provisionr-app/-/milestones'},
                            {label: 'Issues', href: 'https://gitlab.com/provisionr/provisionr-app/-/issues'}
                        ],
                    },
                ],
                copyright: `Copyright © Jefferson Martin. MIT License. Built with Docusaurus and Tailwind CSS.`,
            },
            mermaid: {
                options: {
                    maxTextSize: 99999999,
                },
                theme: { light: 'neutral', dark: 'dark' },
            },
            prism: {
                theme: lightCodeTheme,
                darkTheme: darkCodeTheme
            },
        }),
    themes: ['@docusaurus/theme-mermaid'],
};

module.exports = config;
