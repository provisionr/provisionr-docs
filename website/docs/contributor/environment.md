---
displayed_sidebar: contributorSidebar
sidebar_position: 2
---

# Environment Configuration

For extensive development, you should follow the `provisionr-app` [INSTALL.mac-nginx.md](https://gitlab.com/provisionr/provisionr-app/-/blob/main/INSTALL.mac-nginx.md) instructions to clone Provisionr on your local machine for development and testing.
