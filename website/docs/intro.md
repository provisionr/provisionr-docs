---
displayed_sidebar: introSidebar
slug: /
---

# Introduction

Welcome to the Provisionr community! Our documentation is broken into different categories based on what you want to do.

## Overview

Provisionr is an open source self-service automation platform for dev/test, sandbox, and training cloud infrastructure provisioning with streamlined role based access control (RBAC)

After signing in with Okta, we use your Okta user profile attributes (department, job title, etc.) to auto-assign roles and permissions and allow you to self-service provision infrastructure (AWS account, GCP project, GitLab instance group or user, training labs, etc.) without needing to contact your Infrastructure or IT team to manually provision resources for you.

## API Docs

If you want to integrate with Provisionr or access data using the REST API, our API documentation explains how to get started and provides usage instructions for all of our endpoints.

See the [API Documentation](/api/overview) to learn more.

## Architecture Docs

If you want to learn how Provisionr works behind the scenes, the architecture documentation will provide a great overview and let you explore all the components and how they work.

As we iterate, we identify functionality that could be improved with subject matter expertise and document the current state, desired future state, and any design challenges that we've identified so far.

We welcome feedback with a Security Researcher lens, however encourage contributing design proposals to be able to take action without requiring developers to have as deep of a security headspace.

See [Architecture Documentation](/architecture/overview) to learn more.

## Contributor Docs

Our contributor documentation introduces you to the fundamentals of the [Laravel](https://laravel.com/docs), [Laravel Zero](https://laravel-zero.com), and [Laravel Actions](https://laravelactions.com) frameworks and provides getting started training and tutorials. After you complete the tutorials, you'll be able to easily add an API endpoint, add a CLI command, add a background job script, add a feature to the UI, or create a report using vendor API calls.

See [Contributor Documentation](/contributor/overview) to learn more.
