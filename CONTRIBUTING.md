## Repository Download

1. Open your Terminal/iTerm application and clone the repository to your local machine in the directory where you store your other repositories.
    > Replace `~/Code` with the path to your repositories (ex. `~/Sites`, `~/Projects`, etc.)

    ```
    cd ~/Code
    git clone git@gitlab.com:provisionr/provisionr-docs.git
    ```

2. Use VS Code or your preferred IDE to open the repository.
    ```
    cd provisionr-docs
    code .
    ```

## System Dependencies

1. Install the [Homebrew](https://brew.sh/) MacOS package manager if you haven't already.

    > You can skip this step if you already use `brew` commands on your machine.

    ```
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    ```

2. Install the [NPM] Node.js package manager.

    ```
    brew install node
    ```

3. Install the NPM package dependencies

    ```
    cd ~/Code/provisionr-docs/website
    npm install
    ```

    > If you experience any errors, try running `npm update` instead.

4. Start the local preview web server for Docusaurus.

    > You will get an error message if you run this in the repository directory. You need to navigate to the `website/` child folder to run this command.

    ```
    cd ~/Code/provisionr-docs/website
    npx docusaurus start
    ```

5. Navigate in your browser to http://localhost:3000/provisionr-docs/.

6. Keep your Terminal open for automatic page refresh as you save files in your local repository.

7. All build steps are handled by the CI pipeline so you do not need to worry about compiling files before committing.

8.  You're all set!
